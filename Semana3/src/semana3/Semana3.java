/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana3;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Fabio
 */
public class Semana3 {

    public static int leerint(String mensaje) {
        System.out.print(mensaje);
        Scanner sc = new Scanner(System.in);
        int n2 = sc.nextInt();
        return n2;
    }

    public static String leertexto(String mensaje) {
        System.out.print(mensaje);
        Scanner sc = new Scanner(System.in);
        String n2 = sc.nextLine();
        return n2;
    }

    public static void main(String[] args) {

        // While
        int contador = 1;
        while (contador <= 10) {
            System.out.println(contador);
            contador++;
        }
        // Ejercicio Imprimir números del 10 al 1
        contador = 10;
        while (contador != 0) {
            System.out.println(contador);
            contador--;
        }
        //Ejemplo tablas de multiplicar
        contador = 0;
        int num = leerint("Digite el número de la tabla que desea: ");
        while (contador != 10) {
            int resultado = num * contador;
            System.out.println(num + " x " + contador + " = " + resultado);
            contador++;
        }
        //Ejercicio Sumatoria
        contador = 1;
        int suma = 0;
        while (contador <= 10) {
            num = leerint("Digite un número " + contador + ": ");
            suma += num;
            contador++;
        }
        System.out.println(suma);
        SALIR:
        while (true) {
            int num1 = leerint("Digite el primer número: ");
            int num2 = leerint("Digite el segundo número: ");
            int opcion = leerint("1. Suma\n"
                    + "2. Resta\n"
                    + "3. Multiplicación\n"
                    + "4. División\n"
                    + "5. Salir\n"
                    + "Digite el número de la opción que desea: ");
            switch (opcion){
            case 1: 
                System.out.println("Suma: " + (num1 + num2));
                break;
            case 2:
                System.out.println("Resta: " + (num1 - num2));
                break;
            case 3:
                System.out.println("Multiplicación: " + (num1 * num2));
                break;
            case 4: 
                System.out.println("División: " + (num1 / num2));
                break;
            case 5:
                break SALIR;
            }

        }

    }

}
