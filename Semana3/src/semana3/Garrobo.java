/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana3;

import java.util.Scanner;

/**
 *
 * @author estudiante
 */
public class Garrobo {
  
    //atributos primero
    //luego metodos1.constructor
    //get y set
    //atributos son variables de tipo de dato
    private String nombre;
    private double distancia;
    private double tiempo;
    //constructor
    //para iniciar con valores que yo quiera
    public Garrobo(){
        //hay constructores vacios
        //nombre="wili";
//        distancia=0.0;
//        tiempo=0.0;
        
    }
    //constructor sobrecargado
    public Garrobo(String nombre,double distancia,double tiempo){
        this.nombre=nombre;
        this.distancia=distancia;
        this.tiempo=tiempo;
    }
    
    //metodos publicos
    //menos privado + publico
    //double void no retorna
    //constructorrr
    
    /**
     * Objetivo del método: Calcula el tiempo que tarda el garrobo en recorrer 
     * una distancia dada por parámetro, a partir de la velocidad configurada.
     * @param distancia Tipo double de la distancia que desea recorrer
     * @return Tipo double con el tiempo que dura recorriendo la distancia
     */
    public double calculartiempo(double distancia){
        //fuera y dentro del if devolver un return
        //referencia al atributo un this
        double velocidad=this.distancia/tiempo;
       //regla de tres
       double tiempo=distancia/velocidad;
       return tiempo;
        
    }
    public double calcularDistancia(double tiempo){
        double velocidad=this.distancia/tiempo;
        return velocidad * tiempo;
        
        
    }//gatters and setters
    //get obtener el valor del atrbuto
    //alt+insert para colocarlos mas facil
    public String getNombre(){
        return nombre;
    }
    public double getDistancia(){
        return distancia;
    }
    public double getTiempo(){
        return tiempo;
        
    }
    //set ponerle un valor,asignan un valor pero 
    //no requieren devolver nada,como ell set no devulve nada se usa "void
    public void setNombre(String nombre){
        this.nombre=nombre;
        
    }
    public void setDistancia(double distancia){
        this.distancia=distancia;
    }
    public void setTiempo(double tiempo){
        this.tiempo=tiempo;
    }
    
    public String toString(){
        return "Garrobo = {Nombre="+nombre+" Distancia "+distancia+" tiempo "+tiempo+"}";
    }
         }
    
    
     
    
    
         

