/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimientographics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 *
 * @author ALLAN
 */
public class Raqueta {

    private int x;
    private int y;
    private char m1;
    private char m2;
    private Color color1;
    private Color color2;

    public Raqueta(int x, int y, Color color1, Color color2, char m1, char m2) {
        this.x = x;
        this.y = y;
        this.color1 = color1;
        this.color2 = color2;
        this.m1 = m1;
        this.m2 = m2;
    }

    public void mover(char keyChar) {
        if (keyChar == m1) {
            x -= 10;
        } else if (keyChar == m2) {
            x += 10;
        }
    }

    public void paint(Graphics2D g) {
        g.setColor(color1);
        g.fillRect(x, y, 30, 15);
        g.setColor(color2);
        g.fillRect(x + 30, y, 30, 15);
        g.setColor(color1);
        g.fillRect(x + 60, y, 30, 15);
    }

    public Rectangle getBounds(int pos) {
        switch (pos) {
            case 1:
                return new Rectangle(x, y, 30, 15);
            case 2:
                return new Rectangle(x + 30, y, 30, 15);
            case 3:
                return new Rectangle(x + 60, y, 30, 15);
            default:
                return null;
        }
    }
}
