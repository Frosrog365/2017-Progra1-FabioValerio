/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimientographics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

/**
 *
 * @author ALLAN
 */
public class Circulo {

    private Color color;
    private int diametro = 50;
    private int x;
    private int y;
    private int direccion;
    private int speedX;
    private int speedY;
    private boolean rebotar;
    private boolean rotar;

    public Circulo() {
        this.speedX = 2;
    }

    public Circulo(Color circulo, int x, int y, boolean rebotar, boolean rotar) {
        this.color = circulo;
        this.x = x;
        this.y = y;
        this.speedY = 0;
        this.speedX = 0;
        this.rebotar = true;
        this.rotar = rotar;
    }

    public void mover(int keyChar) {
        switch (keyChar) {
            case KeyEvent.VK_UP:
                //direccion = 1;
                speedY--;
                break;
            case KeyEvent.VK_DOWN:
                //direccion = 2;
                speedY++;
                break;
//            case KeyEvent.VK_LEFT:
//                //direccion = 3;
//                speedX--;
//                break;
//            case KeyEvent.VK_RIGHT:
//                speedX++;
//                //direccion = 4;
//                break;
            case KeyEvent.VK_S:
                direccion = 0;
                break;
            case KeyEvent.VK_A:
                speedX++;
                break;
            case KeyEvent.VK_B:
                speedX--;
                break;

        }

    }

    public void mover(int ancho, int alto) {
        x += speedX;
        y += speedY;

        if (rebotar) {
            if (x <= 0 && speedX < 0) {
                speedX *= -1;
            }
            if (x >= ancho - diametro && speedX > 0) {
                speedX *= -1;
            }
        }
        if (rotar) {
            if (y >= alto + diametro && speedY > 0) {
                y = 0 - diametro;
            }

            if (y <= 0 - diametro && speedY < 0) {
                y = alto;
            }
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Circulo{" + "color=" + color + ", x=" + x + ", y=" + y + '}';
    }

    void paint(Graphics2D g) {
        g.fillOval(x, y, 50, 50);
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, diametro, diametro);
    }

    public void choque(Raqueta j1, Raqueta j2) {
        if (getBounds().intersects(j1.getBounds(1))) {
            mover(KeyEvent.VK_DOWN);
            mover(KeyEvent.VK_LEFT);
        }else if (getBounds().intersects(j1.getBounds(2))) {
            mover(KeyEvent.VK_DOWN);
        }else if (getBounds().intersects(j1.getBounds(3))) {
            mover(KeyEvent.VK_DOWN);
            mover(KeyEvent.VK_RIGHT);
        }
        
        if (getBounds().intersects(j2.getBounds(1))) {
            mover(KeyEvent.VK_UP);
            mover(KeyEvent.VK_LEFT);
        }else if (getBounds().intersects(j2.getBounds(2))) {
            mover(KeyEvent.VK_UP);
        }else if (getBounds().intersects(j2.getBounds(3))) {
            mover(KeyEvent.VK_UP);
            mover(KeyEvent.VK_RIGHT);
        }
    }

}
