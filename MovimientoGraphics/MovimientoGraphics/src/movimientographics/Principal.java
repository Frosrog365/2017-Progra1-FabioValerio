/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimientographics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
class Principal extends JPanel implements KeyListener {

    private Circulo c;
    private Raqueta r1;
    private Raqueta r2;

    public Principal() {
        c = new Circulo(Color.GREEN, 200, 200, true, false);
        r1 = new Raqueta(250, 0, Color.CYAN, Color.BLUE, 'z', 'c');
        r2 = new Raqueta(250, 550, Color.GREEN, Color.GRAY, 'j', 'l');
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(Color.BLACK);
        g.fillRect(600, 600, getWidth(), getHeight());
        c.choque(r1, r2);
        c.mover(getWidth(), getHeight());
        c.paint(g2d);
        r1.paint(g2d);
        r2.paint(g2d);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        c.mover(ke.getKeyCode());
        repaint();
        r1.mover(ke.getKeyChar());
        r2.mover(ke.getKeyChar());
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
