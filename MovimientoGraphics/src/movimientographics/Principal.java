/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimientographics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author estudiante
 */
public class Principal extends JPanel implements KeyListener{
    private Cuadrado c;

    public Principal(){
        c = new Cuadrado(Color.YELLOW, 200, 200);
        setFocusable(true);
        addKeyListener(this);
    }
    @Override
    public void paint(Graphics g){
        super.paint(g);
        g.setColor(c.getColor());
        g.fillRect(c.getX(), c.getY(), 100, 100);
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    
}
