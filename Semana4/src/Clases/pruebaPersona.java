/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.Scanner;

/**
 *
 * @author estudiante
 */
public class pruebaPersona {

    public static String leertexto(String mensaje) {
        System.out.print(mensaje);
        Scanner sc = new Scanner(System.in);
        String n2 = sc.nextLine();
        return n2;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nombre = leertexto("Digite su nombre: ");
        String sexoString = leertexto("Digite su sexo H/F: ");
        int edad = Integer.parseInt(leertexto("Digite su edad: "));
        double peso = Double.parseDouble(leertexto("Digite su peso: "));
        double altura = Double.parseDouble(leertexto("Digite su altura"));

        char sexo = sexoString.charAt(0);
        Persona persona1 = new Persona(nombre, edad, sexo, peso, altura);
        

        System.out.println(persona1.calcularIMC(peso, altura));
        System.out.println(persona1.esMayorDeEdad(edad));
        System.out.println(persona1);
        Persona persona2 = new Persona(nombre, edad, sexo);
        
        System.out.println(persona2.calcularIMC(peso, altura));
        System.out.println(persona2.esMayorDeEdad(edad));
        System.out.println(persona2);
        Persona persona3 = new Persona();
        
        System.out.println(persona3.calcularIMC(peso, altura));
        System.out.println(persona3.esMayorDeEdad(edad));
        System.out.println(persona3);

    }
}
