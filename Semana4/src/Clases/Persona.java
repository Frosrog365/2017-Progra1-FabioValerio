/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author estudiante
 */
public class Persona {

    private String nombre;
    private int edad;
    private int cedula;
    private char sexo;
    private double peso;
    private double altura;

    public Persona() {
        nombre = "";
        edad = 0;
        sexo = 'H';
        peso = 0.0f;
        altura = 0.0f;
        cedula = generaCedula();
    }

    public Persona(String nombre, int edad, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        comprobarSexo();
        this.cedula = generaCedula();
    }

    public Persona(String nombre, int edad, char sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.cedula = generaCedula();
        this.sexo = sexo;
        comprobarSexo();
        this.peso = peso;
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public int getCedula() {
        return cedula;
    }

    public char getSexo() {
        return sexo;
    }

    public double getPeso() {
        return peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double calcularIMC(double peso, double altura) {
        int pesoBajo = -1;
        int pesoNormal = 0;
        int sobrePeso = 1;
        double pesoideal = peso / (Math.pow(altura, 2));
        if (pesoideal < 20) {
            System.out.println(pesoBajo);
            return pesoBajo;
        } else if (pesoideal >= 20 && pesoideal <= 25) {
            System.out.println(pesoNormal);
            return pesoNormal;
        } else {
            System.out.println(sobrePeso);
            return sobrePeso;
        }
    }

    public boolean esMayorDeEdad(int edad) {
        if (edad >= 18) {
            return true;
        } else {
            return false;
        }
    }

    public void comprobarSexo() {
        if (sexo != 'H' && sexo != 'F') {
            this.sexo = 'H';
        }
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", edad=" + edad + ", cedula=" + cedula + ", peso=" + peso + ", altura=" + altura + '}';
    }

    
/**
 * 
 * @return 
 */
    public int generaCedula() {
        int random = (int) (Math.random() * 11111111) + 79999999;
        return random;

    }
}
