/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import sun.java2d.loops.DrawLine;
import sun.java2d.loops.FillRect;

/**
 *
 * @author Fabio
 */
public class FondoLineas extends JPanel implements KeyListener{
    private Color c;
    private Color d;

    public FondoLineas() {
        this.c = Color.BLACK;
        this.d = Color.WHITE;
        setFocusable(true);
        addKeyListener(this);
    }
    @Override
    public void paint(Graphics g){
        g.setColor(c);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(d);
        g.fillOval(100, 100, 100, 100);
        }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_1:
                c = Color.CYAN;
                repaint();
                break;
            case KeyEvent.VK_2:
                c = Color.BLUE;
                repaint();
                break;
            case KeyEvent.VK_3:
                c = Color.YELLOW;
                repaint();
                break;
            case KeyEvent.VK_4:
                c = Color.RED;
                repaint();
                break;
            case KeyEvent.VK_5:
                c = Color.orange;
                repaint();
                break;
            case KeyEvent.VK_6:
                d = Color.CYAN;
                repaint();
                break;
            case KeyEvent.VK_7:
                d = Color.YELLOW;
                repaint();
                break;
            case KeyEvent.VK_8:
                d = Color.MAGENTA;
                repaint();
                break;
            case KeyEvent.VK_9:
                d = Color.GREEN;
                repaint();
                break;
            default:
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }
}
