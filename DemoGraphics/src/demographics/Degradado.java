/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.plaf.metal.MetalLookAndFeel;

/**
 *
 * @author Fabio
 */
public class Degradado extends JPanel {

    @Override
    public void paint(Graphics g) {
        int azul = 250;
        int verde = 100;
        int rojo = 250;
        int p = getHeight() / 256;
        for (int i = 0; i < (getWidth()+getHeight()); i++) {
            rojo = i * 256 / (getWidth()+getHeight());
            g.setColor(new Color(rojo, verde, azul));
            g.drawLine(0,i, i,0);
        }
    }
}
