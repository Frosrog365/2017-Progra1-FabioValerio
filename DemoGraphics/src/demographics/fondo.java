/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Color;
import java.awt.Graphics;
import static javafx.scene.paint.Color.color;
import javax.swing.JPanel;

/**
 *
 * @author estudiante
 */
//crear extends de jpanel
public class fondo extends JPanel{//herencia
    //estoy heredando de un panel
    //dibujar algo
    //metodo
    @Override//sobreescribir
    public void paint(Graphics g){//import del grap
        super.paint(g);
        //drawline:lineas
       // g.drawLine(10, 10, 100, 100);//se manipula en coordenadas xy
       // g.drawLine(10,10,10,100);//estas son formas basicas pero es mejor en for
        //drawoval:circulos
       //setcolor(color.blue):para color
       //g.drawRect(20, 200, 100, 100);//el fillrect relleno
       //color nuevo
       g.setColor(new Color(151,190,135));
       g.fillRect(200, 200, 250, 250);
        for (int i = 0; i < 10; i++) {
            if(i%2==0){
                g.setColor(Color.black);
            }else{
                g.setColor(Color.blue);
            }
            g.drawLine(10*i+20, 10+20, 100*i, 100);
            g.drawOval(10*i, 10*i, 50, 50);
            
        }
        
    }
    
    
}
