/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana1;

import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author Fabio
 */
public class Semana1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Formas de imprimir cosas
//        System.out.println("\u00A9");
//        System.out.println("Hola Mundo!!");
//        System.out.println("AQUI SE ESCRIBIRA EL TEXTO DE CADA CASO");
//        System.out.println("A\tB\tC\n1\t2\t3\n");
//        System.out.println("\"HOLA\"");
//        System.out.println("El caracter de escape \\n cambia de linea");
//        System.out.println("EL UNICODE \\u20A1 vale \u20A1");
//        System.out.println("EL UNICODE \\u00D1 vale \u00D1");
//        
//        // Variables
//        // Siempre inician en minuscula, con formato camelCase
//        int nombreVariable;
//        nombreVariable = 0;
//        System.out.println(nombreVariable);
//        boolean isReal = true;
//        byte b = 122;
//        short s = -29000;
//        int i = 100000;
//        long l = 999999999999L;
//        float f1 = 234.99F;
//        double d;
//        char cvalue = '4';
//        
// 
//        
//        //constantes
//        /* Se usa el "final" para indicar que no se puede cambiar, se escribe el
//        nombre de la constante todo en mayuscula
//        */
//        final int AÑO_NACIMIENTO = 1995;
//        final double PI = 3.1415926;
//        /* Casteo*/
//        int temp = (int)PI;
//        double te = 1;
//        System.out.println(te);
//        
//        // Operadores aritmeticos
//        int x = 12;
//        System.out.println(5+x);
//        System.out.println(5-x);
//        System.out.println(5*x);
//        System.out.println(x/3);
//        System.out.println(x%2);

        // Operadores Unarios ++,--
        // Insertar datos
//        String nombre;
//        nombre = JOptionPane.showInputDialog("Digite su nombre: ");
//        System.out.println(nombre);
//        JOptionPane.showMessageDialog(null
//                , "ERES EL VISITANTE NUMERO 999999999 GANASTE!!"
//                ,"GANASTE"
//                ,JOptionPane.WARNING_MESSAGE);
//        // Imprimir datos
//        JOptionPane.showMessageDialog(null, "El mensaje", "INFORMACION",
//                JOptionPane.INFORMATION_MESSAGE);
//        JOptionPane.showMessageDialog(null,"Error!", "ERROR",
//                JOptionPane.ERROR_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Cuidado!", "Advertencia",
//                JOptionPane.WARNING_MESSAGE);
//        JOptionPane.showMessageDialog(null, "Sin icono","MENSAJE",
//                JOptionPane.PLAIN_MESSAGE);
//        
//        int annoActual;
//        annoActual = 2017;
//        int mesActual;
//        mesActual = 5;
//        int diaActual;
//        diaActual = 19;
//                
//        String anno;
//        String mes;
//        String dia;
//        anno = JOptionPane.showInputDialog("Digite el año de nacimiento: ");
//        mes = JOptionPane.showInputDialog("Digite el mes de nacimiento: ");
//        dia = JOptionPane.showInputDialog("Digite el dia de nacimiento: ");
//                
//        int annoU = Integer.parseInt(anno);
//        int mesU = Integer.parseInt(mes);
//        int diaU = Integer.parseInt(dia);
//        
//        int annosU = annoActual - annoU;
//        int mesesU = mesActual - mesU;
//        int diasU = diaActual - diaU;
//        
//       JOptionPane.showMessageDialog(null
//               ,(nombre+"\n"+"Su edad es de: "+annosU+" años "+mesesU
//                       +" meses "+diasU+" días")
//               , "SU EDAD",
//                JOptionPane.INFORMATION_MESSAGE);
//        System.out.println(5==5);
//        System.out.println(5==6);
//        System.out.println(5!=6);
//        System.out.println(5>6);
//        System.out.println(5<=6);
//        System.out.println(6<=6);
//        
//        boolean x = true;
//        boolean y = false;
//        System.out.println(x && y);
//        x=true;
//        y=true;
//        System.out.println(x && y);
//        
        // Estructura de condicion
//        System.out.println("Estructuras de condicion");
////        int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite su edad: "));
////        boolean res = edad >=18;
////
////        if (res) {
////            System.out.println("Tiene permiso");
////        }  else{
////            System.out.println("No tiene permiso");
////        }
//        String texto = JOptionPane.showInputDialog("Digite un número: ");
//        if (texto != null) {
//            int x = Integer.parseInt(texto);
//            if (x % 2 == 0) {
//                System.out.println("Variable x = " + x + " es par");
//            } else {
//                System.out.println("Variable x = " + x + " es impar");
//            }
//        }
//        int x1 = Integer.parseInt(JOptionPane.showInputDialog("Digite el primer número: "));
//        int y1 = Integer.parseInt(JOptionPane.showInputDialog("Digite el segundo número"));
//        if (x1 < y1) {
//            System.out.println("Variable x= " + x1 + " es menor que y= " + y1);
//        } else if (x1 > y1) {
//            System.out.println("Variable x= " + x1 + " es mayor que y= " + y1);
//        } else {
//            System.out.println("Variable x=" + x1 + " y " + "Variable y=" + y1 + " son iguales");
//        }
//        // Ejercicio 1
//        int numero;
//        numero = Integer.parseInt(JOptionPane.showInputDialog("Digite un número: "));
//        if (numero < 0) {
//            System.out.println("Es negativo");
//        } else{
//            System.out.println("Es Positivo");
//        }
//        if (numero % 5 == 0) {
//            System.out.println("Es divisible entre 5");
//        } else {
//            System.out.println("No es divisble entre 5");
        //}
        //**********************************************************************
        // Ejercicio 2: Generar números aleatorios de 1 a 6
//        int numMax = 6;
//        int numMin = 1;
//        int random = (int)(Math.random()*numMax)+numMin;
//        System.out.println("El dado sacó: "+random);
//        
//        // Segunda forma de crear Random
//        int aleatorio =(int)(Math.random()*6)+1;
//        Random r = new Random();
//        aleatorio = r.nextInt(6)+1;
//        System.out.println(aleatorio);
        //**********************************************************************
//        //Ejercicio 3
//        int cantidadPasajeros = Integer.parseInt(JOptionPane.showInputDialog(
//                "Digite el número de pasajeros: "));
//        int numeroEjes = Integer.parseInt(JOptionPane.showInputDialog(
//                "Digite el número de ejes: "));
//        double costoVehiculo = Integer.parseInt(JOptionPane.showInputDialog(
//                "Digite el costo del vehiculo: "));
//        
//        
//        double impuesto = (costoVehiculo * 0.01);
//        double costoTotal = costoVehiculo + impuesto;
//        if (cantidadPasajeros<20){
//            costoTotal += (impuesto * 0.01);
//        }
//        else if((cantidadPasajeros >=20) && (cantidadPasajeros <=60)){
//            costoTotal += (impuesto * 0.05);
//        }
//        else{
//            costoTotal += (impuesto * 0.08);
//        }
//        
//        if (numeroEjes == 2){
//            costoTotal += (impuesto * 0.05);
//        }
//        else if(numeroEjes == 3){
//            costoTotal += (impuesto * 0.10);
//        }
//        else if (numeroEjes>3){
//            costoTotal += (impuesto * 0.15);
//        }
//        JOptionPane.showMessageDialog(null, ("El costo total del vehículo es 
        //de: "+ costoTotal), 
//                "Costo de Vehículo", JOptionPane.INFORMATION_MESSAGE);
        /* ********************************************************************/
        //Ejercicio 4 - Notas
//        int nota = Integer.parseInt(JOptionPane.showInputDialog(
//                "Digite la nota: "));
//        if (nota >= 90) {
//            JOptionPane.showMessageDialog(null, "Sobresaliente!!", "NOTA DE EXAMEN", JOptionPane.INFORMATION_MESSAGE);
//        } else if (nota >= 80) {
//            JOptionPane.showMessageDialog(null, "Notable!!", "NOTA DE EXAMEN", JOptionPane.INFORMATION_MESSAGE);
//
//        } else if (nota >= 70) {
//            JOptionPane.showMessageDialog(null, "Bien!!", "NOTA DE EXAMEN", JOptionPane.INFORMATION_MESSAGE);
//        } else {
//            JOptionPane.showMessageDialog(null, "Insuficiente", "NOTA DE EXAMEN", JOptionPane.INFORMATION_MESSAGE);
//
//        }
        // Ejercicio 5
//        int canciones = Integer.parseInt(JOptionPane.showInputDialog(
//                "Digite la cantidad de canciones: "));
//        int partituras = Integer.parseInt(JOptionPane.showInputDialog(
//                "Digite la cantidad de partituras: "));
//        
//        if (canciones >=7 && canciones <=10 && partituras == 0){
//            JOptionPane.showMessageDialog(null, "Músico naciente", "MUSICOS", JOptionPane.INFORMATION_MESSAGE);
//        }
//        else if ((canciones >=7 && canciones<=10) && (partituras>=1 && partituras<=5)){
//            JOptionPane.showMessageDialog(null, "Músico estelar", "MUSICOS", JOptionPane.INFORMATION_MESSAGE);
//    }
//        else if ((canciones >10 && partituras >5)){
//            JOptionPane.showMessageDialog(null, "Músico Consagrado", "MUSICOS", JOptionPane.INFORMATION_MESSAGE);
//        }
//        else{
//            JOptionPane.showMessageDialog(null, "Músico en formación", "MUSICOS", JOptionPane.INFORMATION_MESSAGE);
        //}
        // Ejercicio 6
        int monto = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de dinero: "));
        //int monto = 888;
        int div500 = monto / 500;
        int div200 = ((monto % 500) / 200);
        int div100 = (((monto % 500) % 200) / 100);
        int div50 = ((((monto % 500) % 200) % 100) / 50);
        int div20 = (((((monto % 500) % 200) % 100) % 50) / 20);
        int div10 = ((((((monto % 500) % 200) % 100) % 50) % 20) / 10);
        int div5 = (((((((monto % 500) % 200) % 100) % 50) % 20) % 10) / 5);
        int div2 = ((((((((monto % 500) % 200) % 100) % 50) % 20) % 10) % 5) / 2);
        int div1 = (((((((((monto % 500) % 200) % 100) % 50) % 20) % 10) % 5) % 2) / 1);
        String mensaje = "";
        if (monto >= 500) {
            mensaje += div500 + " Billetes de 500 euros\n";
            monto -= (div500 * 500);
        }
        if (monto >= 200) {
            mensaje += div200 + " Billetes de 200 euros\n";
            monto -= (div200 * 200);
        }
        if (monto >= 100) {
            mensaje += div100 + " Billetes de 100 euros\n";
            monto -= (div100 * 100);
        }
        if (monto >= 50) {
            mensaje += div50 + " Billetes de 50 euros\n";
            monto -= (div50 * 50);
        }
        if (monto >= 20) {
            mensaje += div20 + " Billetes de 20 euros\n";
            monto -= (div20 * 20);
        }
        if (monto >= 10) {
            mensaje += div10 + " Billetes de 10 euros\n";
            monto -= (div10 * 10);
        }
        if (monto >= 5) {
            mensaje += div5 + " Billetes de 5 euros\n";
            monto -= (div5 * 5);
        }
        if (monto >= 2) {
            mensaje += div2 + " Monedas de 2 euros\n";
            monto -= (div2 * 2);
        }
        if (monto >= 1) {
            mensaje += div1 + " Monedas de 1 euros\n";
            monto -= (div1 * 1);
        }
        JOptionPane.showMessageDialog(null, mensaje, "CAJERO EUROPEO", JOptionPane.INFORMATION_MESSAGE);

//Profesor
//int monto = Integer.parseInt(JOptionPane.showInputDialog("Digite el dinero: "));
//int monedas = monto/500;
//monto%=500;
//if(monedas>0){
//    System.out.println(monedas+" de 500");
//}
//monedas = monto/200;
//monto%=200;
//if(monedas>0){
//    System.out.println(monedas+" de 200");
//}
//monedas = monto/100;
//monto%=100;
//if(monedas>0){
//    System.out.println(monedas+" de 100");
//}
//monedas = monto/50;
//monto%=50;
//if(monedas>0){
//    System.out.println(monedas+" de 50");}
//monedas = monto/20;
//monto%=20;
//if(monedas>0){
//    System.out.println(monedas+" de 10");}
//monedas = monto/10;
//monto%=10;
//if(monedas>0){
//    System.out.println(monedas+" de 5");}
//monedas = monto/5;
//monto%=5;
//if(monedas>0){
//    System.out.println(monedas+" de 2");}
//monedas = monto/2;
//monto%=2;
//if(monedas>0){
//    System.out.println(monedas+" de 1");}
//monedas = monto/1;
//monto%=1;
    }
}
