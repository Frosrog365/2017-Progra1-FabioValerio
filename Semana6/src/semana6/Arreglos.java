/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana6;

import java.awt.PageAttributes;
import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Arreglos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int arreglo[] = new int[5]; // Declarado e inicializado
        arreglo[2] = 10;
        arreglo[4] = -4;
        arreglo[arreglo.length - 1] = 8;
//        System.out.println(arreglo.length);

//        for (int i = 0; i < arreglo.length; i++) {
//            System.out.print(arreglo[i] + " ");
//        }
        Persona p = new Persona("Fabio", "Valerio", 207420324);
        Persona personas[] = new Persona[10];
        personas[0] = new Persona("Edgardo", "Valerio", 203810578);
        personas[4] = p;
//        for (int i = 0; i < personas.length; i++) {
//            System.out.println(personas[i]);
//        }
        int[] arregloPromedio = {1,2,3,4,5};
        EjerciciosArreglos ejercicios = new EjerciciosArreglos();
        System.out.println(ejercicios.Ejercicio1(arregloPromedio));
        
        int[] arregloMayor = {2,4,3,1,9,8};
        System.out.println(ejercicios.Ejercicio2(arregloMayor));
        
        int[] arregloImpares = {5,2,6,3,7,8,9};
        System.out.println(ejercicios.Ejercicio3(arregloImpares));
        
        int[] encontrarArreglo = {8,5,3,6,4,2,7,22,15,63};
        int numero = Integer.parseInt(JOptionPane.showInputDialog("Digite un número: "));
        System.out.println(ejercicios.Ejercicio4(encontrarArreglo, numero));
        System.out.println(ejercicios.Ejercicio5(encontrarArreglo, numero));
        
        int[] arregloAInvertir = {1,2,3,4,5};
        System.out.println(ejercicios.print(ejercicios.Ejercicio6(arregloAInvertir)));
        System.out.println(ejercicios.print(ejercicios.Ejercicio7(arregloAInvertir)));
        
    }

}
