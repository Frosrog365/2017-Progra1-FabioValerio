/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana6;

/**
 *
 * @author Fabio
 */
public class EjerciciosArreglos {
    

    /**
     * Recibir una lista, sumar cada uno de sus valores y luego dividirlo entre
     * la cantidad de valores Recibe una lista de valores enteros
     *
     * @return El promedio de los valores de la lista
     */
    public int Ejercicio1(int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        return suma / arreglo.length;
    }

    /**
     * Sacar el número mayor un arreglo Recibe una lista con valores enteros
     *
     * @return El número mayor del arreglo
     */
    public int Ejercicio2(int[] arreglo) {
        int mayor = arreglo[0];
        for (int i = 0; i < arreglo.length; i++) {
            if (mayor < arreglo[i]) {
                mayor = arreglo[i];
            }
        }
        return mayor;
    }

    /**
     * Sumar los números impares que están en el arreglo
     *
     * @param arreglo
     * @return La suma de los números impares
     */
    public int Ejercicio3(int[] arreglo) {
        int sumaImpares = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                sumaImpares += arreglo[i];
            }

        }
        return sumaImpares;
    }

    /**
     * Buscar un elemento dentro del arreglo
     *
     * @param arreglo
     * @param num
     * @return Un valor booleano dependiendo si el elemento está en el arreglo
     */
    public boolean Ejercicio4(int[] arreglo, int num) {
        for (int i = 0; i < arreglo.length; i++) {
            if (num == arreglo[i]) {
                return true;
            }
        }
        return false;
    }
    /**
     * Buscar un elemento dentro del arreglo y devolver la posición del elemento
     * en el arreglo
     * @param arreglo
     * @param num
     * @return Un mensaje con la posición del número si está, si no está; también
     * se indica.
     */
    public int Ejercicio5(int[] arreglo, int num) {
        for (int i = 0; i < arreglo.length; i++) {
            if (num == arreglo[i]) {
                return i;
            }
        }
        return 0;
        
    }
    /**
     * Invertir el arreglo
     * @param arreglo
     * @return Un string con el arreglo invertido
     */
    public int[] Ejercicio6(int[]arreglo){
        int[] arregloInvertido = new int[arreglo.length];
        int cont = 0;
        for (int i = arreglo.length; i > 0; i--) {
            arregloInvertido[cont] = arreglo[i-1];
            cont+=1;
        }
        return arregloInvertido;
    }
    /**
     * Función que agarra un arreglo y mueve un 1 espacio cada elemento (el último elemento va al primer lugar.
     * @param arreglo El arreglo que se va mover
     * @return Un arreglo rotado
     */
    public int[] Ejercicio7(int[]arreglo){
        int [] arregloRotado = new int[arreglo.length];
        arregloRotado[0]=arreglo[arreglo.length-1];
        String arregloRotadoStr = "";
        arregloRotadoStr += arregloRotado[0];
        for (int i = 1; i < arreglo.length; i++) {
            arregloRotado[i]=arreglo[i-1];
            arregloRotadoStr+=arregloRotado[i];
        }
        return arregloRotado;
        
    }
    /**
     * Funcion que sirve para imprimir un arreglo de una manera legible
     * @param arreglo El arreglo que se va a recibir con las variables que el usuario defina
     * @return Un String con el arreglo legible de manera normal
     */
    public String print(int[] arreglo) {
        
        String arregloStr = "";
        for (int i = 0; i < arreglo.length; i++) {
            arregloStr+=arreglo[i]+", ";
            }
        arregloStr+="\b\b";
        return "["+arregloStr+"]";
    }
    
   
   
    
   
        
    
    
   
    }
