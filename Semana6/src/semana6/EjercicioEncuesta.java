/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana6;

import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class EjercicioEncuesta {

    public static void main(String[] args) {
        int opcion = 0;
        opcion = Integer.parseInt(JOptionPane.showInputDialog("A cuantos estudiantes va a entrevistar: "));
        while (opcion > 50 || opcion == 0) {
            opcion = Integer.parseInt(JOptionPane.showInputDialog("A cuantos estudiantes va a entrevistar: "));
        }

        Estudiante[] listaEstudiantes = new Estudiante[opcion];

        for (int i = 0; i < opcion; i++) {
            int numMax = 799999999;
            int numMin = 109990999;
            int cedula = (int) (Math.random() * numMax) + numMin;
            numMax = 2;
            numMin = 1;
            int sexo = (int) (Math.random() * numMax) + numMin;
            numMax = 2;
            numMin = 1;
            int trabaja = (int) (Math.random() * numMax) + numMin;
            numMax = 1000000;
            numMin = 250000;
            int sueldo = (int) (Math.random() * numMax) + numMin;
            if (trabaja == 2) {
                sueldo = 0;
            }

            Estudiante est = new Estudiante(cedula, sexo, trabaja, sueldo);
            listaEstudiantes[i] = est;
        }

        int cantHombres = 0;
        int cantMujeres = 0;
        for (int i = 0; i < listaEstudiantes.length; i++) {
            if (listaEstudiantes[i].getSexo() == 1) {
                cantHombres += 1;
            } else if (listaEstudiantes[i].getSexo() == 2) {
                cantMujeres += 1;
            }
        }

        int promedioHombres = (cantHombres * 100) / opcion;
//        System.out.println("Promedio de Hombres en la Universidad: " + promedioHombres + "%");
        int promedioMujeres = (cantMujeres * 100) / opcion;
//        System.out.println("Promedio de Mujeres en la Universidad: " + promedioMujeres + "%");

        int hombresTrabajan = 0;
        int sueldosH = 0;
        int contH = 0;

        for (int i = 0; i < listaEstudiantes.length; i++) {
            if (listaEstudiantes[i].getSexo() == 1 && listaEstudiantes[i].getTrabaja() == 1) {
                hombresTrabajan += 1;
                sueldosH += listaEstudiantes[i].getSueldo();
                contH += 1;
            }
        }
        int promedioSueldosH = 0;
        if (contH != 0) {
            promedioSueldosH = sueldosH / contH;
        }
        //System.out.println("Cantidad de hombres que trabajan: "+(hombresTrabajan*100)/opcion+"%\n"+
        //"Sueldo Promedio: "+promedioSueldos);

        int MujeresTrabajan = 0;
        int sueldosM = 0;
        int contM = 0;
        for (int i = 0; i < listaEstudiantes.length; i++) {
            if (listaEstudiantes[i].getSexo() == 2 && listaEstudiantes[i].getTrabaja() == 1) {
                MujeresTrabajan += 1;
                sueldosM += listaEstudiantes[i].getSueldo();
                contM += 1;
            }
        }
        int promedioSueldosM = 0;
        if (contM != 0) {
            promedioSueldosM = sueldosM / contM;
        }
//        System.out.println("Cantidad de mujeres que trabajan: "+(MujeresTrabajan*100)/opcion+"%\n"+
//                "Sueldo Promedio: "+promedioSueldos);
        JOptionPane.showMessageDialog(null, ("Promedio de Hombres en la Universidad: " + promedioHombres + "%\n"
                + "Promedio de Mujeres en la Universidad: " + promedioMujeres + "%\n"
                + "Porcentaje de hombres que trabajan: " + (hombresTrabajan * 100) / cantHombres + "%\n"
                + "Sueldo Promedio: " + promedioSueldosH + " Colones\n"
                + "Porcentaje de mujeres que trabajan: " + (MujeresTrabajan * 100) / cantMujeres + "%\n"
                + "Sueldo Promedio: " + promedioSueldosM + " Colones\n"), "Encuesta", JOptionPane.INFORMATION_MESSAGE);

    }

}
