package practicamoverfiguras;

import java.awt.Color;
import java.awt.event.KeyEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Fabio_2
 */
public class Cuadrado {

    public Color color;
    public int x;
    public int y;

    @Override
    public String toString() {
        return "Cuadrado{" + "color=" + color + ", x=" + x + ", y=" + y + '}';
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Cuadrado(Color color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public Cuadrado() {
    }

    public void mover(int keyChar) {
        switch (keyChar) {
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }
    }
}
