/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicamoverfiguras;

import java.awt.Color;

/**
 *
 * @author Fabio_2
 */
public class Rombo {
    private Color color;
    private int x;
    private int y;

    @Override
    public String toString() {
        return "Rombo{" + "color=" + color + ", x=" + x + ", y=" + y + '}';
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Rombo(Color color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public Rombo() {
    }
}
