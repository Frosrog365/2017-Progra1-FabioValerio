/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicamoverfiguras;

import java.awt.Color;

/**
 *
 * @author Fabio_2
 */
public class Triangulo {

    private Color color;
    private int x;
    private int y;
    public int xpointsT[];
    public int ypointsT[];

    public Triangulo(Color color, int x, int y,int[] xpointsT, int[] ypointsT) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.xpointsT = xpointsT;
        this.ypointsT = ypointsT;
    }

    @Override
    public String toString() {
        return "Triangulo{" + "color=" + color + ", x=" + x + ", y=" + y + '}';
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Triangulo() {
    }
}
