/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicamoverfiguras;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Fabio_2
 */
public class FondoGraficos extends JPanel implements KeyListener {

    private Color fondo;
    private Cuadrado cuadrado1;
    private Triangulo t;
    private Rombo r;
    private int figura;

    public FondoGraficos() {
        fondo = Color.BLACK;
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(fondo);
        g.fillRect(0, 0, getWidth(), getHeight());
        cuadrado1 = new Cuadrado(Color.BLUE, 100, 100);
        g.setColor(cuadrado1.getColor());
        g.fillRect(150, 150, cuadrado1.getX(), cuadrado1.getY());
//        int xpointsT[] = {60, 105, 150};
//        int ypointsT[] = {60, 10, 60};
//        int npointsT = 3;
//        t = new Triangulo(Color.YELLOW, 100, 100, xpointsT, ypointsT);
//        g.setColor(t.getColor());
//        g.fillPolygon(t.xpointsT, t.ypointsT, npointsT);
//        g.setColor(Color.RED);
//        Polygon poligono = new Polygon();
//        poligono.addPoint(125, 100);
//        poligono.addPoint(100, 125);
//        poligono.addPoint(125, 150);
//        poligono.addPoint(150, 125);
//        g.fillPolygon(poligono);

    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        System.out.println(ke.getKeyCode());
        figura = 1;
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_C:
                figura = 1;
                break;
            case KeyEvent.VK_T:
                figura = 2;
                break;
            case KeyEvent.VK_R:
                figura = 3;
                break;
            default:
                break;
        }
        cambiarColor(ke.getKeyCode());
    }

    public void cambiarColor(int tecla) {
        Color temp = Color.WHITE;
        switch (tecla) {
            case KeyEvent.VK_0:
                temp = Color.RED;
                break;
        }
        switch (figura) {
            case 1:
                cuadrado1.setColor(temp);
                
                break;
        }
        repaint();

    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }
}
