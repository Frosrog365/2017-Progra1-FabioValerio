/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicamoverfiguras;

import javax.swing.JFrame;

/**
 *
 * @author Fabio_2
 */
public class PracticaMoverFiguras {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        FondoGraficos f = new FondoGraficos();
        JFrame frm = new JFrame();// Creamos un formulario
        frm.add(f);
        frm.setSize(400,400); // Le damos un tamaño a la ventana
        frm.setVisible(true); // Hace visible la ventana
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Accion por defecto al cerrar
        frm.setLocationRelativeTo(null); //Centrar la ventana
    }
    
}
