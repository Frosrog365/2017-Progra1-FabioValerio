/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadas;

/**
 *
 * @author Fabio
 */
public class Lista {

    private Nodo primero;

    //Obtener en una posición (índice)
    public Nodo posicion(int num) {
        int cont = 0;
        Nodo temp = primero;
        while (temp != null) {
            if (num == cont) {
                return temp;
            }
            cont++;
            temp = temp.getSig();
        }
        return null;
    }

    //Obtener el último
    //Obtener la posición de un valor
    public int posValor(String dato) {
        int pos = 0;
        Nodo aux = primero;
        while (aux != null) {
            if (aux.getDato().equals(dato)) {
                return pos;
            }
            aux = aux.getSig();
            pos++;
        }
        return pos;
    }

    public int posValorConPos(String dato, int pos) {
        Nodo aux = posicion(pos);
        while (aux != null) {
            if (aux.getDato().equals(dato)) {
                return pos;
            }
            aux = aux.getSig();
            pos++;
        }
        return pos;

    }

    //Reemplazar dato en una posición
    public Nodo reemplazarPos(String dato, int pos) {
        Nodo aux = posicion(pos);
        aux.setDato(dato);
        return aux;
    }

    //Insertar al inicio, al final, en una posición
    //Eliminar el primero, el ultimo, en una posición
    //Elimina el primero
    public Nodo eliminarPrimero() {
        Nodo aux = primero;
        aux = aux.getSig();
        return aux;
    }

    //Elimina el último
    public String eliminarultimo() {
        Nodo aux = primero;
        Lista nuevo = new Lista();
        while (aux != null) {
            if (aux.getSig() != null) {
                nuevo.insertar(aux.getDato());
            }
            aux = aux.getSig();
        }
        return nuevo.getPrimero().toString();
    }

    //Elimina dependiendo de una posición
    public String eliminarPosicion(int posicion) {
        Nodo aux = primero;
        Lista nuevo = new Lista();
        int cont = 0;
        while (cont < tamanno()) {
            if (posicion != cont) {
                nuevo.insertar(posicion(cont).getDato());
            }
            cont++;
        }
        return nuevo.getPrimero().toString();
    }

    //Ordenar
    //Tamaño de la lista
    public int tamanno() {
        int cont = 0;
        Nodo temp = primero;
        while (temp != null) {
            cont++;
            temp = temp.getSig();

        }
        return cont;
    }

    //Invertir una lista
    //Obtener el primero
    public void insertar(String dato) {
        Nodo nuevo = new Nodo(dato);
        if (estaVacia()) {
            primero = nuevo;
        } else {
            Nodo aux = primero;
            while (aux.getSig() != null) {
                aux = aux.getSig();
            }
            aux.setSig(nuevo);
        }

    }

    public boolean estaVacia() {
        return primero == null;
    }

    public Nodo getPrimero() {
        return primero;
    }

}
