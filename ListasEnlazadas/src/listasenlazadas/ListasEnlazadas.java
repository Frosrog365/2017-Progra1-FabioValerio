/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadas;

/**
 *
 * @author Fabio
 */
public class ListasEnlazadas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Nodo n = new Nodo("Hola");
//        Nodo n1 = new Nodo("Perro");
//        Nodo n2 = new Nodo("Gato");
//        n.setSig(n1);
//        n1.setSig(n2);
//        System.out.println(n);
//
//        Nodo temp = n;
//        while (temp != null) {
//            System.out.println(temp.getDato());
//            temp = temp.getSig();
//        }

        Lista lista = new Lista();
        lista.insertar("Hola");
        lista.insertar("Perro");
        lista.insertar("Gato");
        lista.insertar("Perico");
        lista.insertar("Caballo");
//        System.out.println(lista.getPrimero());
//        System.out.println(lista.tamanno());
//        System.out.println(lista.posicion(4));
//        System.out.println(lista.posValor("Perico"));
//        System.out.println(lista.posValorConPos("Gato", 3));
        System.out.println(lista.posicion(0));
        System.out.println(lista.reemplazarPos("Paco", 0));
        System.out.println(lista.reemplazarPos("Hola", 0));
        System.out.println(lista.eliminarPrimero());
        System.out.println(lista.eliminarultimo());
        System.out.println(lista.eliminarPosicion(3));
        
    }
    
}
