/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semanau_practica;

import java.util.Scanner;


/**
 *
 * @author estudiante
 */
public class PruebaClases {

   public static String leertexto(String mensaje) {
        System.out.print(mensaje);
        Scanner sc = new Scanner(System.in);
        String n2 = sc.nextLine();
        return n2;
    }
    public static void main(String[] args) {
        Circunferencia Rueda = new Circunferencia(10.2);
        Circunferencia Moneda = new Circunferencia(1.4);
        System.out.println("El área de la Rueda es de: "+Rueda.calcularArea());
        System.out.println("El área de la Moneda es de : "+Moneda.calcularArea());
        System.out.println("El perímetro de la Rueda es de: "+Rueda.calcularPerimetro());
        System.out.println("El perímetro de la Moneda es de: "+Moneda.calcularPerimetro());
        
        //*********************************************************************
//        double anchoVentana = Double.parseDouble(leertexto("Digite el ancho de la ventana: "));
//        double largoVentana = Double.parseDouble(leertexto("Digite el largo de la ventana: "));
//        double anchoPared = Double.parseDouble(leertexto("Digite el ancho de la pared: "));
//        double largoPared = Double.parseDouble(leertexto("Digite el ancho de la pared: "));
        
        
//        Rectangulo Ventana = new Rectangulo(anchoVentana, largoVentana);
//        Rectangulo Pared = new Rectangulo(largoPared, anchoPared);
//        double areaPared = Pared.calcularArea();
//        double areaVentana = Ventana.calcularArea();
//        
//        double areaPintura = areaPared - areaVentana;
//        double tiempoPintar = (10 * areaPintura)/60;
//        System.out.println("El tiempo que demorará en pintar la pared es de: "+(int)tiempoPintar+" horas");
        
        Fecha fecha1 = new Fecha(19,7,1995);
        System.out.println(fecha1.diaMesAnno());
        System.out.println(fecha1.mesPalabra());
        
        
        Articulo panCuadrado = new Articulo();
        panCuadrado.setClave(0256);
        panCuadrado.setCantidad(15);
        panCuadrado.setDescripcion("Pan Blanco Bimbo, Tamaño Mediano");
        panCuadrado.setPrecio(860);
        System.out.println(panCuadrado);
        System.out.println("El impuesto de venta es de: "+panCuadrado.calcularIVA());
    }
    
}
