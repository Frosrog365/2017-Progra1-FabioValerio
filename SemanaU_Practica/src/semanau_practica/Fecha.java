/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semanau_practica;

/**
 *
 * @author estudiante
 */
public class Fecha {

    private final int DIA = 9;
    private final int MES = 6;
    private final int ANNO = 2017;

    private int dia;
    private int mes;
    private int anno;

    public Fecha() {

    }

    public Fecha(int dia, int mes, int anno) {
        this.dia = dia;
        this.mes = mes;
        this.anno = anno;
        comprobarFecha(dia, mes, anno);
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public String diaMesAnno() {
        return String.format("%02d", dia) + "/" + String.format("%02d", mes) + "/" + this.anno;
    }

    public String mesPalabra() {
        String mesAPalabra = "";
        switch (this.mes) {
            case 1:
                mesAPalabra = String.format("%02d", dia) + " de enero del " + this.anno;
                break;
            case 2:
                mesAPalabra = String.format("%02d", dia) + " de febrero del " + this.anno;
                break;
            case 3:
                mesAPalabra = String.format("%02d", dia) + " de marzo del " + this.anno;
                break;
            case 4:
                mesAPalabra = String.format("%02d", dia) + " de abril del " + this.anno;
                break;
            case 5:
                mesAPalabra = String.format("%02d", dia) + " de mayo del " + this.anno;
                break;
            case 6:
                mesAPalabra = String.format("%02d", dia) + " de junio del " + this.anno;
                break;
            case 7:
                mesAPalabra = String.format("%02d", dia) + " de julio del " + this.anno;
                break;
            case 8:
                mesAPalabra = String.format("%02d", dia) + " de agosto del " + this.anno;
                break;
            case 9:
                mesAPalabra = String.format("%02d", dia) + " de setiembre del " + this.anno;
                break;
            case 10:
                mesAPalabra = String.format("%02d", dia) + " de octubre del " + this.anno;
                break;
            case 11:
                mesAPalabra = String.format("%02d", dia) + " de noviembre del " + this.anno;
                break;
            case 12:
                mesAPalabra = String.format("%02d", dia) + " de diciembre del " + this.anno;
                break;
        }
        return mesAPalabra;
    }

    public void comprobarFecha(int dia, int mes, int anno) {
        String mensaje = "";
        if (this.dia > 31 || this.mes > 12 || this.dia <= 0 || this.mes <= 0) {
            System.out.println(mensaje = "Fecha no válida");
            this.dia = DIA;
            this.mes = MES;
            this.anno = ANNO;
        }

    }
}
