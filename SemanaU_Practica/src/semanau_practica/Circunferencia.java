/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semanau_practica;

/**
 *
 * @author estudiante
 */
public class Circunferencia {
    private double radio;

    public Circunferencia(double radio) {
        this.radio = radio;
    }
    public double calcularArea(){
        double area = Math.PI * (Math.pow(this.radio, 2));
        return area;
    }
    
    public double calcularPerimetro(){
        double perimetro = 2*Math.PI*this.radio;
        return perimetro;
    }
    public void modificarRadio(double radio){
        this.radio = radio;
    }
    public double consultarRadio(){
        return this.radio;
    }
}
