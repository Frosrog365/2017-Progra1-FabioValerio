/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana7;



/**
 *
 * @author Fabio
 */
public class Logica {

    public Logica() {
        
    }

    public static String montoParaCiudad(int monto, Ciudad[] listaCiudades) {
        String ciudadstr = "";
        for (int i = 0; i < listaCiudades.length; i++) {
            if (monto >= listaCiudades[i].getTarifa()) {
                ciudadstr += listaCiudades[i].getNombre() + ", ";
            }
            else{
                ciudadstr = "No alcanza para viajar a ningún lado";
            }

        }

        return ciudadstr + "\b\b";

    }
    public static String tarifaMasAlta(Ciudad[] listaCiudades){
        int mayor = listaCiudades[0].getTarifa();
        String ciudadMasCara = listaCiudades[0].getNombre();
        for (int i = 1; i < listaCiudades.length; i++) {
            if (mayor<listaCiudades[i].getTarifa()){
                mayor = listaCiudades[i].getTarifa();
                ciudadMasCara = listaCiudades[i].getNombre();
            }
        }
        return "La tarifa mas alta es la de: "+ciudadMasCara;
    }
}
