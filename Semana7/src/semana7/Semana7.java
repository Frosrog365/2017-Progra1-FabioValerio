/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana7;

import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Semana7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Ciudad[] listaCiudades = new Ciudad[10];
        listaCiudades[0] = new Ciudad("Alajuela", 11850);
        listaCiudades[1] = new Ciudad("San José", 2000);
        listaCiudades[2] = new Ciudad("Limón", 2800);
        listaCiudades[3] = new Ciudad("Puntarenas", 2700);
        listaCiudades[4] = new Ciudad("Guanacaste", 3000);
        listaCiudades[5] = new Ciudad("Cartago", 1700);
        listaCiudades[6] = new Ciudad("Pital", 750);
        listaCiudades[7] = new Ciudad("Aguas Zarcas", 470);
        listaCiudades[8] = new Ciudad("Venecia", 600);
        listaCiudades[9] = new Ciudad("Fortuna", 700);
        int monto = Integer.parseInt(JOptionPane.showInputDialog("Digite el monto con el que va a viajar: "));
        System.out.print("Usted podría viajar a: ");
        Logica l = new Logica();
        System.out.println(l.montoParaCiudad(monto,listaCiudades));
        System.out.println(l.tarifaMasAlta(listaCiudades));
    }
    
}
