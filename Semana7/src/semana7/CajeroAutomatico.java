package semana7;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author estudiante
 */
public class CajeroAutomatico {

    private int saldo;
    private int[] billetes;
    private int[] dinero;

    public CajeroAutomatico() {
        saldo = 1000;
        billetes = new int[]{10000, 5000, 2000, 1000, 500, 100};
        dinero = new int[]{0, 0, 0, 0, 0, 0};
    }

    public CajeroAutomatico(int saldo, int[] billetes, int[] dinero) {
        this.saldo = saldo;
        this.billetes = new int[]{10000, 5000, 2000, 1000, 500, 100};
        this.dinero = dinero;
    }

    public int getSaldo() {
        return saldo;
    }
    public int deposito(int monto){
        this.saldo+=monto;
        return saldo;
    }
    
    public boolean retiro(int monto){
        if ((this.saldo-monto)>0){
            this.saldo-=monto;
            desgloseDinero();
            return true;
        }
        else{
            return false;
        }
    }
    public String desgloseDinero(){
        this.dinero[0] = this.saldo / 10000;
        this.dinero[1] = ((this.saldo % 10000) / 5000);
        this.dinero[2] = (((this.saldo % 10000) % 5000) / 2000);
        this.dinero[3] = ((((this.saldo % 10000) % 5000) % 2000) / 1000);
        this.dinero[4] = (((((this.saldo % 10000) % 5000) % 2000) % 1000) / 500);
        this.dinero[5] = ((((((this.saldo % 10000) % 5000) % 2000) % 1000) % 500) / 100);
        return "Vector Dinero: ["+this.dinero[0]+"|"+this.dinero[1]+"|"+this.dinero[2]+"|"+this.dinero[3]+"|"+this.dinero[4]+"|"
                +this.dinero[5]+"]";
       
    }
    public String imprimirVectorBilletes(){
        return "Vector Billetes: ["+10000+"|"+5000+"|"+2000+"|"+1000+"|"+500+"|"
                +100+"]";
    }

}
