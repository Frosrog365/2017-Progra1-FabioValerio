/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1progra1_fabiovalerio;

import javax.swing.JOptionPane;

/**
 *
 * @author Fabio
 */
public class Tarea1Progra1_FabioValerio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int precioCrucero = Integer.parseInt(JOptionPane.showInputDialog("Digite el precio del crucero: "));
        int cantPersonas = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de personas que van a viajar: "));
        double descuento = 0;
        if (cantPersonas == 1) {
            int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite su edad: "));
            if (edad >= 18 && edad <= 30) {
                descuento = precioCrucero * 0.078;
            } else if (edad > 30) {
                descuento = precioCrucero * 0.1;
            }
        } else if (cantPersonas == 2) {
            descuento = precioCrucero * 0.115;
            precioCrucero -= descuento;
            System.out.println("El precio de su viaje con descuento es de: " + precioCrucero*2);
        } else if (cantPersonas >= 3) {
            descuento = precioCrucero * 0.15;
            precioCrucero -= descuento;
            System.out.println("El precio de su viaje con descuento es de: " + precioCrucero*cantPersonas);
        } else if (cantPersonas == 0) {
            System.out.println("No puede escribir que van \"0\" pasajeros");
        }

        

// TODO code application logic here
    }

}
