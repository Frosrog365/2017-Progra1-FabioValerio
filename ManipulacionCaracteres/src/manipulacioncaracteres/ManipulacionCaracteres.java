/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author Fabio
 */
public class ManipulacionCaracteres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String str = "Hola Mundo";
        // |H|o|l|a| |M|u|n|d|o|
        // |0|1|2|3|4|5|6|7|8|9|
        //equals --> Comparar dos Strings, True: Idénticos
        System.out.println(str);
        if (str.equals("Hola Mundo")) { //No usar el "==" en Strings, se usa el .equals()
            System.out.println("Son iguales");
        }
        //equalsIgnoreCase --> Compara dos Strings, sin importar mayusculas o minusculas
        if (str.equalsIgnoreCase("hola mundo")) { //Igual que el equals solo que ignora si hay mayusculas o minusculas
            System.out.println("Son iguales, sin importar mayúsculas");
        }

        //CharAt --> Permite obtener un caracter de una posicion especifica
        System.out.println(str.charAt(5));

        // lenght --> Permitir obtener el largo de un string
        System.out.println(str.length());

        //toCharArray --> Permite convertir de un String a un arreglo de char
        char[] letras = str.toCharArray();
        letras[5] = 'm';

        //new String(char[]) -- > Permite crear un String a partir de un arreglo de char
        String x = new String(letras);
        System.out.println("x --> " + x);

        //indexOf --> Permite buscar una letra o frase dentro de un string
        System.out.println(str.indexOf("o"));

        //indexOf ("String,2)--> Permite buscar una letra o frase dentro de un string, indicando la posicion de inicio
        System.out.println(str.indexOf("o", 2));

        //contains --> Permite saber si un String o letra existe en una frase
        System.out.println(str.contains("Hola"));

        //replace --> Reemplaza un texto o char una frase
        System.out.println(str.replace("Hola", "Buenas"));
        //        str = str.replace("H", "h");
        //        System.out.println(str);

        //replaceAll --> Reemplaza una expresion regular, por un texto o caracter definido
        System.out.println(str.replaceAll("[A-Z]", "X"));

        //matches --> Permite saber si un string cumple con una expresion regular
        String EMAIL_PATTERN
                = "^[A-Za-z0-9+_.-]+@(.+)$";
        str = "fvalerio04@gmail.com";
        System.out.println(str.matches(EMAIL_PATTERN));

        //split --> Permite generar un arreglo de string a partir de un string y un elemento separador
        str = "Azul,Blanco,Rojo,Amarillo,Verde";
        String[] colores = str.split(",");
        for (int i = 0; i < colores.length; i++) {
            System.out.println(colores[i]);
        }
        //substring --> [1,6] --> Obtiene un fragmento del string
        str = "Hola Mundo";
        System.out.println(str.substring(2)); //Desde X hasta el final
        System.out.println(str.substring(2, 7));// Desde X hasta Y

        //concat(+) --> Concatenar Strings
        System.out.println(str.concat(", Fabio"));
        System.out.println(str + ", Fabio");

        //ToLowerCase --> Convierte todo el texto a minuscula
        System.out.println(str.toLowerCase());
        
        //ToUpperCase --> Convierte todo el texto a mayúscula
        System.out.println(str.toUpperCase());
        
    }

}
