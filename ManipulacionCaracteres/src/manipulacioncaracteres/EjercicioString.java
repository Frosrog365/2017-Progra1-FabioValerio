package manipulacioncaracteres;

//Clase que tenga una frase, luego crear los siguientes metodos
//Cantidad de Vocales
//Cantidad de numeros
//Cantidad de Mayusculas
//Invertir una frase
/**
 *
 * @author Fabio
 */
public class EjercicioString {

    private String frase;

    public EjercicioString(String fraseStr) {
        this.frase = frase;
    }

    public String getFrase() {
        return frase;
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }

    public EjercicioString() {
    }

    public int cantidadVocales(String frase) {
        int contVocales = 0;
        frase = frase.toLowerCase();
        char[] vocales = frase.toCharArray();
        for (int i = 0; i < vocales.length; i++) {
            if (vocales[i] == 'a' || vocales[i] == 'e' || vocales[i] == 'i' || vocales[i] == 'o' || vocales[i] == 'u') {
                contVocales++;
            }
        }
        return contVocales;
    }

    public int cantNumeros(String frase) {
        frase = frase.replaceAll("[A-Za-z\\s]", "");
        return frase.length();
    }

    public int cantMayusculas(String frase) {
        frase = frase.replaceAll("[a-z0-9\\s]", "");
        return frase.length();

    }

    public String fraseInvertida(String frase) {
        char[] letras = frase.toCharArray();
        char[] letrasInvertidas = new char[letras.length];
        int j = 0;
        for (int i = letras.length; i == 0; i--) {
            letrasInvertidas[j] = letras[i];
            j++;
        }
        String fraseInv = new String(letrasInvertidas);
        return fraseInv;
    }

}
