/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author Fabio
 */
public class MainEjercicioString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String frase = "Fab789io Valeri456o";
        EjercicioString eS = new EjercicioString(frase);
        System.out.println(eS.cantidadVocales(frase));
        System.out.println(eS.cantMayusculas(frase));
        System.out.println(eS.cantNumeros(frase));
        System.out.println(eS.fraseInvertida(frase));
    }
    
}
