/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana2;

import javax.swing.JOptionPane;

/**
 *
 * @author Fabio_2
 */
public class CotidianoSemana2 {

    public static void main(String[] args) {
        //Ejercicio 2: Números del 0 a 99
        int num = Integer.parseInt(JOptionPane.showInputDialog("Digite un número del 1 al 99: "));
        int decenas = num / 10;
        int unidades = num % 10;
        String decenasLetras = "";
        String numLetras = "";
        switch (num) {
            case 11:
                numLetras += "Once";
                System.out.println(numLetras);
                break;
            case 12:
                numLetras += "Doce";
                System.out.println(numLetras);
                break;
            case 13:
                numLetras += "Trece";
                System.out.println(numLetras);
                break;
            case 14:
                numLetras += "Catorce";
                System.out.println(numLetras);
                break;
            case 15:
                numLetras += "Quince";
                System.out.println(numLetras);
                break;
            case 16:
                numLetras += "Dieciseis";
                System.out.println(numLetras);
                break;
            case 17:
                numLetras += "Diecisiete";
                System.out.println(numLetras);
                break;
            case 18:
                numLetras += "Dieciocho";
                System.out.println(numLetras);
                break;
            case 19:
                numLetras += "Diecinueve";
                System.out.println(numLetras);
                break;
            default:
                switch (decenas) {
                    case 1:
                        decenasLetras = "Diez";
                        break;
                    case 2:
                        decenasLetras = "Veinte";
                        break;
                    case 3:
                        decenasLetras = "Treinta";
                        break;
                    case 4:
                        decenasLetras = "Cuarenta";
                        break;
                    case 5:
                        decenasLetras = "Cincuenta";
                        break;
                    case 6:
                        decenasLetras = "Sesenta";
                        break;
                    case 7:
                        decenasLetras = "Setenta";
                        break;
                    case 8:
                        decenasLetras = "Ochenta";
                        break;
                    case 9:
                        decenasLetras = "Noventa";
                        break;
                }
                String unidadesLetras = "";
                switch (unidades) {
                    case 1:
                        unidadesLetras += "uno";
                        break;
                    case 2:
                        unidadesLetras += "dos";
                        break;
                    case 3:
                        unidadesLetras += "tres";
                        break;
                    case 4:
                        unidadesLetras += "cuatro";
                        break;
                    case 5:
                        unidadesLetras += "cinco";
                        break;
                    case 6:
                        unidadesLetras += "seis";
                        break;
                    case 7:
                        unidadesLetras += "siete";
                        break;
                    case 8:
                        unidadesLetras += "ocho";
                        break;
                    case 9:
                        unidadesLetras += "nueve";
                }
                if (num % 10 == 0) {
                    System.out.println(decenasLetras);
                } else {
                    System.out.println(decenasLetras + " y " + unidadesLetras);
                }
        }
        int monto = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de dinero: "));
        int div500 = monto / 500;
        int div200 = ((monto % 500) / 200);
        int div100 = (((monto % 500) % 200) / 100);
        int div50 = ((((monto % 500) % 200) % 100) / 50);
        int div20 = (((((monto % 500) % 200) % 100) % 50) / 20);
        int div10 = ((((((monto % 500) % 200) % 100) % 50) % 20) / 10);
        int div5 = (((((((monto % 500) % 200) % 100) % 50) % 20) % 10) / 5);
        int div2 = ((((((((monto % 500) % 200) % 100) % 50) % 20) % 10) % 5) / 2);
        int div1 = (((((((((monto % 500) % 200) % 100) % 50) % 20) % 10) % 5) % 2) / 1);

        switch (div500) {
            case 0:
            default:
                System.out.println(div500 + " Billetes de 500 euros");
        }
        switch (div200) {
            case 0:
            default:
                System.out.println(div200 + " Billetes de 200 euros");
        }
        switch (div100) {
            case 0:
            default:
                System.out.println(div100 + " Billetes de 100 euros");
        }
        switch (div50) {
            case 0:
            default:
                System.out.println(div50 + " Billetes de 50 euros");
        }
        switch (div20) {
            case 0:
            default:
                System.out.println(div20 + " Billetes de 20 euros");
        }
        switch (div10) {
            case 0:
            default:
                System.out.println(div10 + " Billetes de 10 euros");
        }
        switch (div5) {
            case 0:
            default:
                System.out.println(div5 + " Billetes de 5 euros");
        }
        switch (div2) {
            case 0:
            default:
                System.out.println(div2 + " Monedas de 2 euros");
        }
        switch (div1) {
            case 0:
            default:
                System.out.println(div1 + " Monedas de 1 euros");

        }
    }
}
