/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana2;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Fabio
 */
public class Semana2 {
//  Metodo para insertar datos en consola

    public static int leerint(String mensaje) {
        System.out.println(mensaje);
        Scanner sc = new Scanner(System.in);
        int n2 = sc.nextInt();
        return n2;
    }

    public static void main(String[] args) {
//        // Ejemplo 1 Switch: Días de la semana
//        int dia = leerint("Digite el dia de la semana: ");
//        //System.out.println(dia);
//        switch (dia) {
//            case 1:
//                System.out.println("Domingo");
//                break;
//            case 2:
//                System.out.println("Lunes");
//                break;
//            case 3:
//                System.out.println("Martes");
//                break;
//            case 4:
//                System.out.println("Miercoles");
//                break;
//            case 5:
//                System.out.println("Jueves");
//                break;
//            case 6:
//                System.out.println("Viernes");
//                break;
//            case 7:
//                System.out.println("Sabado");
//                break;
//            default:
//                System.out.println("Número Inválido");
//        }
//        // EJemplo 2: Switch con los meses
//        int mes = leerint("Digite el número del mes: ");
//        switch (mes) {
//            case 1:
//                System.out.println("Enero");
//                break;
//            case 2:
//                System.out.println("Febrero");
//                break;
//            case 3:
//                System.out.println("Marzo");
//                break;
//            case 4:
//                System.out.println("Abril");
//                break;
//            case 5:
//                System.out.println("Mayo");
//                break;
//            case 6:
//                System.out.println("Junio");
//                break;
//            case 7:
//                System.out.println("Julio");
//                break;
//            case 8:
//                System.out.println("Agosto");
//                break;
//            case 9:
//                System.out.println("Setiembre");
//                break;
//            case 10:
//                System.out.println("Octubre");
//                break;
//            case 11:
//                System.out.println("Noviembre");
//                break;
//            case 12:
//                System.out.println("Diciembre");
//                break;
//            default:
//                System.out.println("Número Inválido");
//        }
//
//        // EJemplo 3: Switch con impresion de filas sin break
//        int filas = leerint("Digite la cantidad de filas: ");
//        switch (filas) {
//            case 5:
//                System.out.println("************************");
//            case 4:
//                System.out.println("************************");
//            case 3:
//                System.out.println("************************");
//            case 2:
//                System.out.println("************************");
//            case 1:
//                System.out.println("************************");
//        }
//        //Ejercicio 1: Calculadora
//        int num1 = leerint("DIgite el primer número");
//        int num2 = leerint("Digite el segundo número:");
//        System.out.println("1. Suma");
//        System.out.println("2. Resta");
//        System.out.println("3. Multiplicación");
//        System.out.println("4. División");
//        int opcion = leerint("Digite la opción que desea utilizar:");
//        switch (opcion) {
//            case 1:
//                System.out.println("Suma: " + num1 + "+" + num2 + "=" + (num1 + num2));
//                break;
//            case 2:
//                System.out.println("Resta: " + num1 + "-" + num2 + "=" + (num1 - num2));
//                break;
//            case 3:
//                System.out.println("Multiplicación: " + num1 + "x" + num2 + "=" + (num1 * num2));
//                break;
//            case 4:
//                System.out.println("División: " + num1 + "/" + num2 + "=" + (num1 / num2));
//                break;
//
//        }
        //Ejercicio 2: Números del 0 a 99
        int num = Integer.parseInt(JOptionPane.showInputDialog("Digite un número del 1 al 99: "));
        int decenas = num / 10;
        int unidades = num % 10;
        String decenasLetras = "";
        String numLetras = "";
        switch (num) {
            case 11:
                numLetras += "Once";
                System.out.println(numLetras);
                break;
            case 12:
                numLetras += "Doce";
                System.out.println(numLetras);
                break;
            case 13:
                numLetras += "Trece";
                System.out.println(numLetras);
                break;
            case 14:
                numLetras += "Catorce";
                System.out.println(numLetras);
                break;
            case 15:
                numLetras += "Quince";
                System.out.println(numLetras);
                break;
            case 16:
                numLetras += "Dieciseis";
                System.out.println(numLetras);
                break;
            case 17:
                numLetras += "Diecisiete";
                System.out.println(numLetras);
                break;
            case 18:
                numLetras += "Dieciocho";
                System.out.println(numLetras);
                break;
            case 19:
                numLetras += "Diecinueve";
                System.out.println(numLetras);
                break;
            default:
                switch (decenas) {
                    case 1:
                        decenasLetras = "Diez";
                        break;
                    case 2:
                        decenasLetras = "Veinte";
                        break;
                    case 3:
                        decenasLetras = "Treinta";
                        break;
                    case 4:
                        decenasLetras = "Cuarenta";
                        break;
                    case 5:
                        decenasLetras = "Cincuenta";
                        break;
                    case 6:
                        decenasLetras = "Sesenta";
                        break;
                    case 7:
                        decenasLetras = "Setenta";
                        break;
                    case 8:
                        decenasLetras = "Ochenta";
                        break;
                    case 9:
                        decenasLetras = "Noventa";
                        break;
                }
                String unidadesLetras = "";
                switch (unidades) {
                    case 1:
                        unidadesLetras += "uno";
                        break;
                    case 2:
                        unidadesLetras += "dos";
                        break;
                    case 3:
                        unidadesLetras += "tres";
                        break;
                    case 4:
                        unidadesLetras += "cuatro";
                        break;
                    case 5:
                        unidadesLetras += "cinco";
                        break;
                    case 6:
                        unidadesLetras += "seis";
                        break;
                    case 7:
                        unidadesLetras += "siete";
                        break;
                    case 8:
                        unidadesLetras += "ocho";
                        break;
                    case 9:
                        unidadesLetras += "nueve";
                }
                if (num % 10 == 0) {
                    System.out.println(decenasLetras);
                } else {
                    System.out.println(decenasLetras + " y " + unidadesLetras);
                }
        }
        int monto = Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de dinero: "));
        int div500 = monto / 500;
        int div200 = ((monto % 500) / 200);
        int div100 = (((monto % 500) % 200) / 100);
        int div50 = ((((monto % 500) % 200) % 100) / 50);
        int div20 = (((((monto % 500) % 200) % 100) % 50) / 20);
        int div10 = ((((((monto % 500) % 200) % 100) % 50) % 20) / 10);
        int div5 = (((((((monto % 500) % 200) % 100) % 50) % 20) % 10) / 5);
        int div2 = ((((((((monto % 500) % 200) % 100) % 50) % 20) % 10) % 5) / 2);
        int div1 = (((((((((monto % 500) % 200) % 100) % 50) % 20) % 10) % 5) % 2) / 1);

        switch (div500) {
            case 0:
            default:
                System.out.println(div500 + " Billetes de 500 euros");
        }
        switch (div200) {
            case 0:
            default:
                System.out.println(div200 + " Billetes de 200 euros");
        }
        switch (div100) {
            case 0:
            default:
                System.out.println(div100 + " Billetes de 100 euros");
        }
        switch (div50) {
            case 0:
            default:
                System.out.println(div50 + " Billetes de 50 euros");
        }
        switch (div20) {
            case 0:
            default:
                System.out.println(div20 + " Billetes de 20 euros");
        }
        switch (div10) {
            case 0:
            default:
                System.out.println(div10 + " Billetes de 10 euros");
        }
        switch (div5) {
            case 0:
            default:
                System.out.println(div5 + " Billetes de 5 euros");
        }
        switch (div2) {
            case 0:
            default:
                System.out.println(div2 + " Monedas de 2 euros");
        }
        switch (div1) {
            case 0:
            default:
                System.out.println(div1 + " Monedas de 1 euros");
        }

        String letra = JOptionPane.showInputDialog("Digite una letra entre a y e: ");
        switch (letra) {
            case "A":
            case "a":
                System.out.println("Excelente");
                break;

            case "B":
            case "b":
                System.out.println("Bueno");
                break;

            case "C":
            case "c":
                System.out.println("Regular");
                break;

            case "D":
            case "d":
                System.out.println("Malo");
                break;

            case "E":
            case "e":
                System.out.println("Pésimo");
                break;

        }
    }
}
