/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectomate;

import javax.swing.JOptionPane;

/**
 *
 * @author Fabio
 */
public class ProyectoMate {

    public static void iniciar() {
        String ecuacion = JOptionPane.showInputDialog(null, "Digite la ecuación cuadrática: \n"
                + "(Favor respetar el formato: n x^2 + n x + n)\n"
                + "Donde n son los números", "Análisis de Equaciones Cuadráticas", 1);
        String[] listaNumeros = ecuacion.split(" ");
        int a = Integer.parseInt(listaNumeros[0]);
        int b = Integer.parseInt(listaNumeros[3]);
        int c = Integer.parseInt(listaNumeros[6]);
        if (listaNumeros[2].equals("-")) {
            b *= -1;
        }
        if (listaNumeros[5].equals("-")) {
            c *= -1;
        }
        System.out.println("A: " + a + " " + "B: " + b + " " + "C: " + c);

        float discriminante = (float) Math.pow(b, 2) - 4 * a * c;
        float x1 = (float) ((-b) + (Math.sqrt(discriminante))) / (2 * a);
        float x2 = (float) ((-b) - (Math.sqrt(discriminante))) / (2 * a);
        System.out.println("Cortes con x: " + "(" + x1 + ", 0)" + "(" + x2 + ", 0)");
        System.out.println("Corte con y: " + "(0," + c + ")");
        float ejeSimetria = (float) -b / (2 * a);
        float puntoM = (float) -discriminante / (4 * a);
        System.out.println("Vertice: " + "(" + ejeSimetria + ", " + puntoM + ")");

        if (a < 0) {
            System.out.println("Es concava hacia abajo");
        } else {
            System.out.println("Es concava hacia arriba");
        }

        if (a > 0) {
            System.out.println("Decrece: " + "]-∞," + ejeSimetria + "[");
            System.out.println("Crece: " + "]" + ejeSimetria + ", +∞[");
            System.out.println("Ámbito: " + "[" + puntoM + ", +∞[");
        }

    }

    public static void main(String[] args) {
        while (true) {
            String opcion = JOptionPane.showInputDialog(null, "1. Iniciar\n"
                    + "2. Salir\n" +  "Digite el número de la opción", "Análisis de Equaciones Cuadráticas\n+",
                     1);
            if (opcion.equals("1")) {
                iniciar();
            } else if (opcion.equals("2")) {
                JOptionPane.showMessageDialog(null, "Gracias por su uso", "Análisis de Equaciones Cuadráticas", 1);
                break;
            } else {
                JOptionPane.showMessageDialog(null, "Opción Inválida", "Análisis de Equaciones Cuadráticas", 0);
            }

        }

    }

}
