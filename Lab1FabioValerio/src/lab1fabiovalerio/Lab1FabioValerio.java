/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1fabiovalerio;

import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Lab1FabioValerio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double areaParedes = 0;
        double areaVentanas = 0;
        String nombre="";
        SALIR:
        while (true){
        int opcionMenu = Integer.parseInt(JOptionPane.showInputDialog(null, "1. Nueva Cotización\n2.Salir\nDigite una opción:", "El pintor", 3));
        switch (opcionMenu) {
            case 1:
                nombre = JOptionPane.showInputDialog(null, "Digite el nombre del cliente:", "El pintor", 3);
                double altoPared = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el alto de la pared", "El pintor", 3));
                double anchoPared = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el ancho de la pared", "El pintor", 3));
                Rectangulo pared = new Rectangulo(altoPared, anchoPared);
                areaParedes += pared.calcularArea();
                int opcionVentanas = Integer.parseInt(JOptionPane.showInputDialog(null, "Tiene Ventanas\n1. Si\n2. No", "El pintor", 3));
                switch (opcionVentanas) {
                    case 1:
                        int tipoVentana = Integer.parseInt(JOptionPane.showInputDialog(null, "Tipo de la Ventana\n1. Rectangular\n2. Circular", "El pintor", 3));
                        switch (tipoVentana) {
                            case 1:
                                double altoVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el alto de la ventana", "El pintor", 3));
                                double anchoVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el ancho de la ventana", "El pintor", 3));
                                Rectangulo ventanaCuadrada = new Rectangulo(altoVentana, anchoVentana);
                                areaVentanas += ventanaCuadrada.calcularArea();
                                break;
                            case 2:
                                double radioVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el radio de la ventana", "El pintor", 3));
                                Circunferencia ventanaCircular = new Circunferencia(radioVentana);
                                areaVentanas += ventanaCircular.calcularArea();
                                break;
                        }
                 
                        int opcionMasVentanas = 1;
                        while (opcionMasVentanas == 1) {
                            opcionMasVentanas = Integer.parseInt(JOptionPane.showInputDialog(null, "Tiene más Ventanas\n1. Si\n2. No", "El pintor", 3));
                            if (opcionMasVentanas == 1) {
                                int tipoNuevaVentana = Integer.parseInt(JOptionPane.showInputDialog(null, "Tipo de la Ventana\n1. Rectangular\n2. Circular", "El pintor", 3));
                                if (tipoNuevaVentana == 1) {
                                    double altoVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el alto de la ventana", "El pintor", 3));
                                    double anchoVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el ancho de la ventana", "El pintor", 3));
                                    Rectangulo ventanaCuadrada = new Rectangulo(altoVentana, anchoVentana);
                                    areaVentanas += ventanaCuadrada.calcularArea();
                                } else if (tipoNuevaVentana == 2) {

                                    double radioNuevaVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el radio de la ventana", "El pintor", 3));
                                    Circunferencia ventanaCircular = new Circunferencia(radioNuevaVentana);
                                    areaVentanas += ventanaCircular.calcularArea();
                                }
                            }
                        }
                        int opcionMasParedes = 1;
                        while (opcionMasParedes == 1) {
                            opcionMasParedes = Integer.parseInt(JOptionPane.showInputDialog(null, "Tiene más Paredes\n1. Si\n2. No", "El pintor", 3));
                            if (opcionMasParedes == 1) {
                                int altoNuevaPared = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite el alto de la pared", "El pintor", 3));
                                int anchoNuevaPared = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite el ancho de la pared", "El pintor", 3));
                                Rectangulo Nuevapared = new Rectangulo(altoNuevaPared, anchoNuevaPared);
                                areaParedes += Nuevapared.calcularArea();
                                opcionVentanas = Integer.parseInt(JOptionPane.showInputDialog(null, "Tiene Ventanas\n1. Si\n2. No", "El pintor", 3));
                                switch (opcionVentanas) {
                                    case 1:
                                        tipoVentana = Integer.parseInt(JOptionPane.showInputDialog(null, "Tipo de la Ventana\n1. Rectangular\n2. Circular", "El pintor", 3));
                                        switch (tipoVentana) {
                                            case 1:
                                                double altoVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el alto de la ventana", "El pintor", 3));
                                                double anchoVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el ancho de la ventana", "El pintor", 3));
                                                Rectangulo ventanaCuadrada = new Rectangulo(altoVentana, anchoVentana);
                                                areaVentanas += ventanaCuadrada.calcularArea();
                                                break;
                                            case 2:
                                                double radioVentana = Double.parseDouble(JOptionPane.showInputDialog(null, "Digite el radio de la ventana", "El pintor", 3));
                                                Circunferencia ventanaCircular = new Circunferencia(radioVentana);
                                                areaVentanas += ventanaCircular.calcularArea();
                                                break;
                                        }
                                }

                            }

                        }
                }
            case 2:
                break SALIR;
        }
        double areaAPintar = (int)areaParedes - (int)areaVentanas;
        double tiempoCotizacionHoras = (10 * areaAPintar) / 60;
        double tiempoCotizacionMin = (10 * areaAPintar) % 60;
        String totalHora = (int)tiempoCotizacionHoras+" Horas "+(int)tiempoCotizacionMin+" Minutos";
        if (tiempoCotizacionMin > 1) {
            tiempoCotizacionHoras += 1;
        }
        int cobro = 30 * (int) tiempoCotizacionHoras;
        JOptionPane.showMessageDialog(null, "Cliente: "+nombre+"\n"+
                                            "Total de áreas de paredes: "+(int)areaParedes+"\n"+
                                            "Total de áreas de ventanas: "+(int)areaVentanas+"\n"+
                                            "Total de área a pintar: "+(int)areaAPintar+"\n\n"+
                                            "Tiempo total del trabajo: "+totalHora+"\n"+
                                            "Costo total de la obra: $"+cobro,"El pintor",1);
        
        
    }
        
}
}