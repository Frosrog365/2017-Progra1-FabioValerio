/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebapacman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author estudiante
 */
public class PruebaPacman extends JPanel implements KeyListener {
    
    public Tablero t = new Tablero();
    public Pacman p = new Pacman(25, 25);
    
    public PruebaPacman() {
        setFocusable(true);
        addKeyListener(this);
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        t.pintar(g2d);
        p.paint(g2d);
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
    }
    
    @Override
    public void keyPressed(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                p.setY(p.getY() + 10);
                break;
            case KeyEvent.VK_UP:
                p.setY(p.getY() - 10);
                break;
            case KeyEvent.VK_LEFT:
                p.setX(p.getX()-10);
                break;
            case KeyEvent.VK_RIGHT:
                p.setX(p.getX()+10);
                break;
        }
        repaint();
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
    }
}
