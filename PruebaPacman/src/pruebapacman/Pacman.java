/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebapacman;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Fabio_2
 */
public class Pacman {

    private int x;
    private int y;

    public Pacman() {
    }

    public Pacman(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void paint(Graphics g) {
        g.setColor(Color.yellow);
        g.fillOval(x, y, 20, 20);
    }
}
