/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebapacman;

import javax.swing.JFrame;

/**
 *
 * @author estudiante
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PruebaPacman panel = new PruebaPacman();
        JFrame frm = new JFrame();// Creamos un formulario
        frm.add(panel);
        frm.setSize(601, 601); // Le damos un tamaño a la ventana
        frm.setVisible(true); // Hace visible la ventana
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Accion por defecto al cerrar
        frm.setResizable(false);
        frm.setLocationRelativeTo(null); //Centrar la ventana

    }

}
